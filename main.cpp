#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <dbt.h>
#include <hidsdi.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "ds4_input.h"

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;

LRESULT CALLBACK InputWindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch (uMsg)
    {
    case WM_DEVICECHANGE:
        switch (wParam)
        {
        case DBT_DEVICEARRIVAL:
        {
            DEV_BROADCAST_HDR* header = (DEV_BROADCAST_HDR*)lParam;
            if (header->dbch_devicetype == DBT_DEVTYP_DEVICEINTERFACE)
            {
                DEV_BROADCAST_DEVICEINTERFACE* devIf = (DEV_BROADCAST_DEVICEINTERFACE*) lParam;
                char* devPath = devIf->dbcc_name;
                Ds4OnHidArrival(devPath);
            }
        }
        break;
        case DBT_DEVICEREMOVECOMPLETE:
        {
            //NOTE : DO NOT READ __ANYTHING__ HERE. Device is already removed, only path is known.
            DEV_BROADCAST_HDR* header = (DEV_BROADCAST_HDR*)lParam;
            if (header->dbch_devicetype == DBT_DEVTYP_DEVICEINTERFACE)
            {
                DEV_BROADCAST_DEVICEINTERFACE* devIf = (DEV_BROADCAST_DEVICEINTERFACE*) lParam;
                char* devPath = devIf->dbcc_name;
                Ds4OnHidRemoval(devPath);
            }
        }
        break;
        }
        return TRUE;
    }

    return DefWindowProc(hwnd, uMsg, wParam, lParam);
}

HWND CreateInputWindow()
{
    const char className[] = "InputWindowClass";
    WNDCLASSA wc;
    if (!GetClassInfoA(GetModuleHandleA(NULL), className, &wc))
    {
        WNDCLASSA wc = {};
        wc.lpfnWndProc = InputWindowProc;
        wc.hInstance = GetModuleHandleA(NULL);
        wc.lpszClassName = className;

        RegisterClassA(&wc);
    }

    HWND window = CreateWindowExA(
        0,                      // Optional window styles.
        className,              // Window class
        "InputWindow",         // Window text
        WS_OVERLAPPEDWINDOW,    // Window style
        // Size and position
        CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
        NULL,                   // Parent window    
        NULL,                   // Menu
        GetModuleHandleA(NULL), // Instance handle
        NULL                    // Additional application data
    );

    DEV_BROADCAST_DEVICEINTERFACE_A hidDeviceFilter = {};
    hidDeviceFilter.dbcc_size = sizeof(DEV_BROADCAST_DEVICEINTERFACE_A);
    hidDeviceFilter.dbcc_devicetype = DBT_DEVTYP_DEVICEINTERFACE;
    HidD_GetHidGuid(&hidDeviceFilter.dbcc_classguid);

    HDEVNOTIFY hidDeviceNotify = RegisterDeviceNotificationA(window, &hidDeviceFilter, DEVICE_NOTIFY_WINDOW_HANDLE);

    return window;
}

void InputWindowPump()
{
    MSG msg = {};
    while (PeekMessageA(&msg, NULL, 0, 0, PM_REMOVE))
    {
        TranslateMessage(&msg);
        DispatchMessageA(&msg);
    }
}

#define PRINT_BOOL(a) (a) ? ("Y") : ("N")

u32 gSlot = -1;

void connected(u32 slot, u64 mac, void* extra)
{
    gSlot = slot;
}

void disconnected(u32 slot, u64 mac, void* extra)
{
    gSlot = -1;
}

struct Timer
{
    static double freq;
    LARGE_INTEGER start;
    LARGE_INTEGER end;
    const char* text;

    static void Init()
    {
        LARGE_INTEGER freq;
        QueryPerformanceFrequency(&freq);
        Timer::freq = (freq.QuadPart / 1000.0);
    }

    Timer(const char* text)
    {
        this->text = text;
        QueryPerformanceCounter(&start);
    }

    ~Timer()
    {
        QueryPerformanceCounter(&end);
        printf("%s took :\t%09.6f [ms]\n", text, ((end.QuadPart - start.QuadPart) / freq));
    }
};

double Timer::freq = 0;

int main()
{
    Timer::Init();
    HWND window = CreateInputWindow();
    if (window != INVALID_HANDLE_VALUE)
    {
        Ds4Init(connected, disconnected, nullptr);
        Ds4InState state = {};
        Ds4OutState outState = {};
        outState.ledColorBlue = 128;
        Ds4WriteState(gSlot, &outState);
        while (true)
        {
            COORD coord;
            coord.X = 0;
            coord.Y = 0;
            SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);

            InputWindowPump();

            {
                Timer t("Ds4ReadState");
                Ds4ReadState(gSlot, &state);
            }

            outState = {};
            outState.flags = (DS4_FLAG_SET_LED_COLOR | DS4_FLAG_SET_MOTOR | DS4_FLAG_SET_LED_TIME);
            outState.ledColorRed = state.lTrigger;
            outState.ledColorGreen = state.rTrigger;
            outState.motorLeft = state.lTrigger;
            outState.motorRight = state.rTrigger;
            outState.ledTimeOnIn10MsTicks = 0;
            outState.ledTimeOffIn10MsTicks = 0;

            {
                Timer t("Ds4WriteState");
                Ds4WriteState(gSlot, &outState);
            }

            printf("LeftAnalogX :\t%03d\tLeftAnalogY :\t%03d\nRightAnalogX :\t%03d\tRightAnalogY :\t%03d\n",
                state.lStickX,
                state.lStickY,
                state.rStickX,
                state.rStickY
            );
            printf("LeftTrigger :\t%03d\tRightTrigger :\t%03d\n",
                state.lTrigger,
                state.rTrigger
            );

            printf("Dpad : %03d Up : %s Right : %s Down : %s Left : %s\n", 
                state.buttons & DS4_BUTTON_DPAD_STATE,
                PRINT_BOOL(state.buttons & DS4_BUTTON_UP),
                PRINT_BOOL(state.buttons & DS4_BUTTON_RIGHT),
                PRINT_BOOL(state.buttons & DS4_BUTTON_DOWN),
                PRINT_BOOL(state.buttons & DS4_BUTTON_LEFT)
            );


            printf("Square :\t%s\nX :\t\t%s\nCircle :\t%s\nTriangle :\t%s\n",
                PRINT_BOOL(state.buttons & DS4_BUTTON_SQUARE),
                PRINT_BOOL(state.buttons & DS4_BUTTON_X),
                PRINT_BOOL(state.buttons & DS4_BUTTON_CIRCLE),
                PRINT_BOOL(state.buttons & DS4_BUTTON_TRIANGLE)
            );

            printf("L1 :\t\t%s\nR1 :\t\t%s\nL2 :\t\t%s\nR2 :\t\t%s\n",
                PRINT_BOOL(state.buttons & DS4_BUTTON_L1),
                PRINT_BOOL(state.buttons & DS4_BUTTON_R1),
                PRINT_BOOL(state.buttons & DS4_BUTTON_L2),
                PRINT_BOOL(state.buttons & DS4_BUTTON_R2)
            );

            printf("SHARE :\t\t%s\nOPTIONS :\t%s\nL3 :\t\t%s\nR3 :\t\t%s\nPS :\t\t%s\nTOUCHPAD :\t%s\n",
                PRINT_BOOL(state.buttons & DS4_BUTTON_SHARE),
                PRINT_BOOL(state.buttons & DS4_BUTTON_OPTIONS),
                PRINT_BOOL(state.buttons & DS4_BUTTON_L3),
                PRINT_BOOL(state.buttons & DS4_BUTTON_R3),
                PRINT_BOOL(state.buttons & DS4_BUTTON_PS),
                PRINT_BOOL(state.buttons & DS4_BUTTON_TOUCHPAD)
            );

            Sleep(10);
        }
    }
}
