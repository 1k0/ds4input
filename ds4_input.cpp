#include <windows.h>
#include <hidsdi.h>
#include <setupapi.h>
#include <malloc.h>

#include "ds4_input.h"

#define INTERNAL static
#define FALSE 0

typedef uint64_t u64;
typedef uint32_t u32;
typedef uint16_t u16;
typedef uint8_t u8;

typedef int64_t i64;
typedef int32_t i32;
typedef int16_t i16;
typedef int8_t i8;

typedef float f32;
typedef float norm32;

typedef uint32_t b32;

#define HID_VENDOR_ID_SONY 0x054C
#define HID_PRODUCT_ID_DS4V1 0x05C4
#define HID_PRODUCT_ID_DS4V2 0x09CC

#define NO_DEVICE_SLOT -1
#define DS4_MAX_DEVICES 16
#define NO_DEVICE_MAC u64(0)

#define POLL_INTERVAL_1MS u8(0xC0 | 0x01)
#define POLL_INTERVAL_2MS u8(0xC0 | 0x02)
#define POLL_INTERVAL_62MS u8(0xC0 | 0x3E)

INTERNAL const u32 crcTable[256] =
{
    0x00000000UL, 0x77073096UL, 0xee0e612cUL, 0x990951baUL, 0x076dc419UL,
    0x706af48fUL, 0xe963a535UL, 0x9e6495a3UL, 0x0edb8832UL, 0x79dcb8a4UL,
    0xe0d5e91eUL, 0x97d2d988UL, 0x09b64c2bUL, 0x7eb17cbdUL, 0xe7b82d07UL,
    0x90bf1d91UL, 0x1db71064UL, 0x6ab020f2UL, 0xf3b97148UL, 0x84be41deUL,
    0x1adad47dUL, 0x6ddde4ebUL, 0xf4d4b551UL, 0x83d385c7UL, 0x136c9856UL,
    0x646ba8c0UL, 0xfd62f97aUL, 0x8a65c9ecUL, 0x14015c4fUL, 0x63066cd9UL,
    0xfa0f3d63UL, 0x8d080df5UL, 0x3b6e20c8UL, 0x4c69105eUL, 0xd56041e4UL,
    0xa2677172UL, 0x3c03e4d1UL, 0x4b04d447UL, 0xd20d85fdUL, 0xa50ab56bUL,
    0x35b5a8faUL, 0x42b2986cUL, 0xdbbbc9d6UL, 0xacbcf940UL, 0x32d86ce3UL,
    0x45df5c75UL, 0xdcd60dcfUL, 0xabd13d59UL, 0x26d930acUL, 0x51de003aUL,
    0xc8d75180UL, 0xbfd06116UL, 0x21b4f4b5UL, 0x56b3c423UL, 0xcfba9599UL,
    0xb8bda50fUL, 0x2802b89eUL, 0x5f058808UL, 0xc60cd9b2UL, 0xb10be924UL,
    0x2f6f7c87UL, 0x58684c11UL, 0xc1611dabUL, 0xb6662d3dUL, 0x76dc4190UL,
    0x01db7106UL, 0x98d220bcUL, 0xefd5102aUL, 0x71b18589UL, 0x06b6b51fUL,
    0x9fbfe4a5UL, 0xe8b8d433UL, 0x7807c9a2UL, 0x0f00f934UL, 0x9609a88eUL,
    0xe10e9818UL, 0x7f6a0dbbUL, 0x086d3d2dUL, 0x91646c97UL, 0xe6635c01UL,
    0x6b6b51f4UL, 0x1c6c6162UL, 0x856530d8UL, 0xf262004eUL, 0x6c0695edUL,
    0x1b01a57bUL, 0x8208f4c1UL, 0xf50fc457UL, 0x65b0d9c6UL, 0x12b7e950UL,
    0x8bbeb8eaUL, 0xfcb9887cUL, 0x62dd1ddfUL, 0x15da2d49UL, 0x8cd37cf3UL,
    0xfbd44c65UL, 0x4db26158UL, 0x3ab551ceUL, 0xa3bc0074UL, 0xd4bb30e2UL,
    0x4adfa541UL, 0x3dd895d7UL, 0xa4d1c46dUL, 0xd3d6f4fbUL, 0x4369e96aUL,
    0x346ed9fcUL, 0xad678846UL, 0xda60b8d0UL, 0x44042d73UL, 0x33031de5UL,
    0xaa0a4c5fUL, 0xdd0d7cc9UL, 0x5005713cUL, 0x270241aaUL, 0xbe0b1010UL,
    0xc90c2086UL, 0x5768b525UL, 0x206f85b3UL, 0xb966d409UL, 0xce61e49fUL,
    0x5edef90eUL, 0x29d9c998UL, 0xb0d09822UL, 0xc7d7a8b4UL, 0x59b33d17UL,
    0x2eb40d81UL, 0xb7bd5c3bUL, 0xc0ba6cadUL, 0xedb88320UL, 0x9abfb3b6UL,
    0x03b6e20cUL, 0x74b1d29aUL, 0xead54739UL, 0x9dd277afUL, 0x04db2615UL,
    0x73dc1683UL, 0xe3630b12UL, 0x94643b84UL, 0x0d6d6a3eUL, 0x7a6a5aa8UL,
    0xe40ecf0bUL, 0x9309ff9dUL, 0x0a00ae27UL, 0x7d079eb1UL, 0xf00f9344UL,
    0x8708a3d2UL, 0x1e01f268UL, 0x6906c2feUL, 0xf762575dUL, 0x806567cbUL,
    0x196c3671UL, 0x6e6b06e7UL, 0xfed41b76UL, 0x89d32be0UL, 0x10da7a5aUL,
    0x67dd4accUL, 0xf9b9df6fUL, 0x8ebeeff9UL, 0x17b7be43UL, 0x60b08ed5UL,
    0xd6d6a3e8UL, 0xa1d1937eUL, 0x38d8c2c4UL, 0x4fdff252UL, 0xd1bb67f1UL,
    0xa6bc5767UL, 0x3fb506ddUL, 0x48b2364bUL, 0xd80d2bdaUL, 0xaf0a1b4cUL,
    0x36034af6UL, 0x41047a60UL, 0xdf60efc3UL, 0xa867df55UL, 0x316e8eefUL,
    0x4669be79UL, 0xcb61b38cUL, 0xbc66831aUL, 0x256fd2a0UL, 0x5268e236UL,
    0xcc0c7795UL, 0xbb0b4703UL, 0x220216b9UL, 0x5505262fUL, 0xc5ba3bbeUL,
    0xb2bd0b28UL, 0x2bb45a92UL, 0x5cb36a04UL, 0xc2d7ffa7UL, 0xb5d0cf31UL,
    0x2cd99e8bUL, 0x5bdeae1dUL, 0x9b64c2b0UL, 0xec63f226UL, 0x756aa39cUL,
    0x026d930aUL, 0x9c0906a9UL, 0xeb0e363fUL, 0x72076785UL, 0x05005713UL,
    0x95bf4a82UL, 0xe2b87a14UL, 0x7bb12baeUL, 0x0cb61b38UL, 0x92d28e9bUL,
    0xe5d5be0dUL, 0x7cdcefb7UL, 0x0bdbdf21UL, 0x86d3d2d4UL, 0xf1d4e242UL,
    0x68ddb3f8UL, 0x1fda836eUL, 0x81be16cdUL, 0xf6b9265bUL, 0x6fb077e1UL,
    0x18b74777UL, 0x88085ae6UL, 0xff0f6a70UL, 0x66063bcaUL, 0x11010b5cUL,
    0x8f659effUL, 0xf862ae69UL, 0x616bffd3UL, 0x166ccf45UL, 0xa00ae278UL,
    0xd70dd2eeUL, 0x4e048354UL, 0x3903b3c2UL, 0xa7672661UL, 0xd06016f7UL,
    0x4969474dUL, 0x3e6e77dbUL, 0xaed16a4aUL, 0xd9d65adcUL, 0x40df0b66UL,
    0x37d83bf0UL, 0xa9bcae53UL, 0xdebb9ec5UL, 0x47b2cf7fUL, 0x30b5ffe9UL,
    0xbdbdf21cUL, 0xcabac28aUL, 0x53b39330UL, 0x24b4a3a6UL, 0xbad03605UL,
    0xcdd70693UL, 0x54de5729UL, 0x23d967bfUL, 0xb3667a2eUL, 0xc4614ab8UL,
    0x5d681b02UL, 0x2a6f2b94UL, 0xb40bbe37UL, 0xc30c8ea1UL, 0x5a05df1bUL,
    0x2d02ef8dUL
};

INTERNAL
u32 Ds4Crc32(u32 crc, u8* buf, u64 len)
{
    while (len-- != 0)
        crc = crcTable[((uint8_t)crc ^ *(buf++))] ^ (crc >> 8);
    return crc;
}

#define DS4_FEATURE_REPORT_0x02         0x02    //usb calibration data
#define DS4_FEATURE_REPORT_0x05         0x05    //bt calibration data
#define DS4_FEATURE_REPORT_0x81         0x81    //usb mac
#define DS4_FEATURE_REPORT_0xA3         0xA3    //version info, manufacture time, mac .....
#define DS4_FEATURE_REPORT_0x02_SIZE    37      //usb calibration data
#define DS4_FEATURE_REPORT_0x05_SIZE    41      //bt calibration data
#define DS4_FEATURE_REPORT_0x81_SIZE    7       //usb mac
#define DS4_FEATURE_REPORT_0xA3_SIZE    49      //version info, manufacture time, mac .....

#define DS4_INPUT_REPORT_0x01           0x01    //usb input
#define DS4_INPUT_REPORT_0x11           0x11    //bt input
#define DS4_INPUT_REPORT_0x01_SIZE      64      //usb input
#define DS4_INPUT_REPORT_0x11_SIZE      78      //bt input

#define DS4_OUTPUT_REPORT_0x05          0x05
#define DS4_OUTPUT_REPORT_0x11          0x11
#define DS4_OUTPUT_REPORT_0x05_SIZE     32
#define DS4_OUTPUT_REPORT_0x11_SIZE     78

INTERNAL
void Ds4BtFillCrc32(u8 report[DS4_OUTPUT_REPORT_0x11_SIZE + 1])
{
    u32 crc = ~Ds4Crc32(0xFFFFFFFF, report, (DS4_OUTPUT_REPORT_0x11_SIZE - 4));
    report[75] = u8(crc >> 0);
    report[76] = u8(crc >> 8);
    report[77] = u8(crc >> 16);
    report[78] = u8(crc >> 24);
}

INTERNAL
b32 IsDs4(HANDLE device)
{
    HIDD_ATTRIBUTES attribs;
    if (HidD_GetAttributes(device, &attribs))
    {
        return (
            attribs.VendorID == HID_VENDOR_ID_SONY &&
            (attribs.ProductID == HID_PRODUCT_ID_DS4V1 ||
            attribs.ProductID == HID_PRODUCT_ID_DS4V2)
            );
    }

    return FALSE;
}

//TODO : rewrite
INTERNAL
char* stristr(const char* str1, const char* str2)
{
    const char* p1 = str1;
    const char* p2 = str2;
    const char* r = *p2 == 0 ? str1 : 0;

    while (*p1 != 0 && *p2 != 0)
    {
        if (tolower((unsigned char)*p1) == tolower((unsigned char)*p2))
        {
            if (r == 0)
            {
                r = p1;
            }

            p2++;
        }
        else
        {
            p2 = str2;
            if (r != 0)
            {
                p1 = r + 1;
            }

            if (tolower((unsigned char)*p1) == tolower((unsigned char)*p2))
            {
                r = p1;
                p2++;
            }
            else
            {
                r = 0;
            }
        }

        p1++;
    }

    return *p2 == 0 ? (char*)r : 0;
}

INTERNAL
b32 IsUsbPath(char* devPath)
{
    return (stristr(devPath, "mi_") != nullptr);
}

INTERNAL
void Ds4Enumerate()
{
    GUID hidGuid = {};
    HidD_GetHidGuid(&hidGuid);
    GUID hidClassGUID = { 0x745a17a0, 0x74d3, 0x11d0, 0xb6, 0xfe, 0x00, 0xa0, 0xc9, 0x0f, 0x57, 0xda };

    HDEVINFO devInfoSet = SetupDiGetClassDevsA(&hidGuid, NULL, NULL, (DIGCF_PRESENT | DIGCF_DEVICEINTERFACE));
    if (devInfoSet != INVALID_HANDLE_VALUE)
    {
        SP_DEVICE_INTERFACE_DATA devIf;
        devIf.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);

        DWORD ifIdx = 0;
        while (SetupDiEnumDeviceInterfaces(devInfoSet, NULL, &hidGuid, ifIdx, &devIf))
        {
            DWORD size = 0;
            SetupDiGetDeviceInterfaceDetailA(devInfoSet, &devIf, NULL, 0, &size, NULL);

            SP_DEVICE_INTERFACE_DETAIL_DATA_A* devDetail = (SP_DEVICE_INTERFACE_DETAIL_DATA_A*)HeapAlloc(GetProcessHeap(), 0, size);
            if (devDetail)
            {
                devDetail->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA_A);

                SP_DEVINFO_DATA devData;
                devData.cbSize = sizeof(devData);
                if (SetupDiGetDeviceInterfaceDetailA(devInfoSet, &devIf, devDetail, size, NULL, &devData))
                {
                    if (!memcmp(&devData.ClassGuid, &hidClassGUID, sizeof(GUID)))
                    {
                        Ds4OnHidArrival(devDetail->DevicePath);
                    }
                }

                HeapFree(GetProcessHeap(), 0, devDetail);
            }

            ++ifIdx;
        }
    }
    SetupDiDestroyDeviceInfoList(devInfoSet);
}

struct Ds4Conn
{
    enum Type : u32
    {
        TYPE_NONE = 0,
        TYPE_BT,
        TYPE_USB,
        TYPE_COUNT
    };

    Type type;
    HANDLE device;
    char devPath[MAX_PATH];
};

INTERNAL 
Ds4Conn CreateDs4Conn(char* devPath)
{
    Ds4Conn conn = {};
    
    HANDLE device = CreateFileA(devPath,
        (GENERIC_READ | GENERIC_WRITE),
        (FILE_SHARE_READ | FILE_SHARE_WRITE),
        NULL,
        OPEN_EXISTING,
        0,
        NULL
    );

    if (device != INVALID_HANDLE_VALUE && IsDs4(device))
    {
        conn.type = (IsUsbPath(devPath)) ? 
                        (Ds4Conn::TYPE_USB) : 
                        (Ds4Conn::TYPE_BT);
        conn.device = device;
        strncpy(conn.devPath, devPath, MAX_PATH);
    }
    else
    {
        CloseHandle(device);
    }

    return conn;
}

INTERNAL
void FinishDs4Conn(Ds4Conn* conn)
{
    CloseHandle(conn->device);
    memset(conn, 0x00, sizeof(Ds4Conn));
}

INTERNAL
u64 GetMac(Ds4Conn* conn)
{
    u64 mac = 0;

    if (conn->type == Ds4Conn::TYPE_USB)
    {
        u8 report[DS4_FEATURE_REPORT_0x81_SIZE];
        report[0] = DS4_FEATURE_REPORT_0x81;
        if (HidD_GetFeature(conn->device, report, sizeof(report)))
        {
            mac <<= 8; mac |= report[6];
            mac <<= 8; mac |= report[5];
            mac <<= 8; mac |= report[4];
            mac <<= 8; mac |= report[3];
            mac <<= 8; mac |= report[2];
            mac <<= 8; mac |= report[1];
        }
    }
    else if (conn->type == Ds4Conn::TYPE_BT)
    {
        WCHAR buffer[126];
        buffer[0] = 0x0000;
        if (HidD_GetSerialNumberString(conn->device, buffer, sizeof(buffer)) && wcslen(buffer))
        {
            for (u32 i = 0; i < 12; ++i)
            {
                mac <<= 4;
                u8 c = buffer[i];
                mac |= u64(9 * (c >> 6) + (c & 0x000F));
            }
        }
    }

    return mac;
}

struct Ds4CalibData
{
    u16 gyroPitchBias;
    u16 gyroPitchPlus;
    u16 gyroPitchMinus;
    u16 gyroYawBias;
    u16 gyroYawPlus;
    u16 gyroYawMinus;
    u16 gyroRollBias;
    u16 gyroRollPlus;
    u16 gyroRollMinus;
    u16 gyroSpeedPlus;
    u16 gyroSpeedMinus;
    u16 accXPlus;
    u16 accXMinus;
    u16 accYPlus;
    u16 accYMinus;
    u16 accZPlus;
    u16 accZMinus;
};

INTERNAL
Ds4CalibData GetCalibData(Ds4Conn* conn)
{
    Ds4CalibData calib = {};

    u8 reportId = (conn->type == Ds4Conn::TYPE_USB) ? (DS4_FEATURE_REPORT_0x02) : (DS4_FEATURE_REPORT_0x05);
    u64 reportSize = (conn->type == Ds4Conn::TYPE_USB) ? (DS4_FEATURE_REPORT_0x02_SIZE) : (DS4_FEATURE_REPORT_0x05_SIZE);
    u8* reportBuffer = (u8*) _alloca(reportSize);
    reportBuffer[0] = reportId;

    if (HidD_GetFeature(conn->device, reportBuffer, reportSize))
    {
        if (conn->type == Ds4Conn::TYPE_USB)
        {
            u8 reportId = reportBuffer[0];

            calib.gyroPitchBias =   *(u16*) (&reportBuffer[1]);
            calib.gyroYawBias =     *(u16*) (&reportBuffer[3]);
            calib.gyroRollBias =    *(u16*) (&reportBuffer[5]);
            calib.gyroPitchPlus =   *(u16*) (&reportBuffer[7]);
            calib.gyroPitchMinus =  *(u16*) (&reportBuffer[9]);
            calib.gyroYawPlus =     *(u16*) (&reportBuffer[11]);
            calib.gyroYawMinus =    *(u16*) (&reportBuffer[13]);
            calib.gyroRollPlus =    *(u16*) (&reportBuffer[15]);
            calib.gyroRollMinus =   *(u16*) (&reportBuffer[17]);
            calib.gyroSpeedPlus =   *(u16*) (&reportBuffer[19]);
            calib.gyroSpeedMinus =  *(u16*) (&reportBuffer[21]);
            calib.accXPlus =        *(u16*) (&reportBuffer[23]);
            calib.accXMinus =       *(u16*) (&reportBuffer[25]);
            calib.accYPlus =        *(u16*) (&reportBuffer[27]);
            calib.accYMinus =       *(u16*) (&reportBuffer[29]);
            calib.accZPlus =        *(u16*) (&reportBuffer[31]);
            calib.accZMinus =       *(u16*) (&reportBuffer[33]);
        }
        else if (conn->type == Ds4Conn::TYPE_BT)
        {
            u8 reportId = reportBuffer[0];

            calib.gyroPitchBias =   *(u16*) (&reportBuffer[1]);
            calib.gyroYawBias =     *(u16*) (&reportBuffer[3]);
            calib.gyroRollBias =    *(u16*) (&reportBuffer[5]);
            calib.gyroPitchPlus =   *(u16*) (&reportBuffer[7]);
            calib.gyroYawPlus =     *(u16*) (&reportBuffer[9]);
            calib.gyroRollPlus =    *(u16*) (&reportBuffer[11]);
            calib.gyroPitchMinus =  *(u16*) (&reportBuffer[13]);
            calib.gyroYawMinus =    *(u16*) (&reportBuffer[15]);
            calib.gyroRollMinus =   *(u16*) (&reportBuffer[17]);
            calib.gyroSpeedPlus =   *(u16*) (&reportBuffer[19]);
            calib.gyroSpeedMinus =  *(u16*) (&reportBuffer[21]);
            calib.accXPlus =        *(u16*) (&reportBuffer[23]);
            calib.accXMinus =       *(u16*) (&reportBuffer[25]);
            calib.accYPlus =        *(u16*) (&reportBuffer[27]);
            calib.accYMinus =       *(u16*) (&reportBuffer[29]);
            calib.accZPlus =        *(u16*) (&reportBuffer[31]);
            calib.accZMinus =       *(u16*) (&reportBuffer[33]);
        }
    }

    return calib;
}

struct Ds4Device
{
    u64 mac;
    Ds4Conn btConn;
    Ds4Conn usbConn;
    Ds4CalibData calibData;
};
Ds4Device gDeviceSlots[DS4_MAX_DEVICES];
Ds4OnConnected gOnConnected; 
Ds4OnDisconnected gOnDisconnected;
void* gExtra;

INTERNAL
i32 GetDeviceSlot(u64 mac)
{
    for (i32 i = 0; i < DS4_MAX_DEVICES; ++i)
    {
        if (gDeviceSlots[i].mac == mac)
        {
            return i;
        }
    }

    return NO_DEVICE_SLOT;
}

INTERNAL
Ds4Conn* GetDs4Conn(char* devPath, u32* slot = NULL)
{
    for (i32 i = 0; i < DS4_MAX_DEVICES; ++i)
    {
        if (!_strnicmp(gDeviceSlots[i].btConn.devPath, devPath, MAX_PATH))
        {
            if (slot) (*slot) = i;
            return &gDeviceSlots[i].btConn;
        }
        if (!_strnicmp(gDeviceSlots[i].usbConn.devPath, devPath, MAX_PATH))
        {
            if (slot) (*slot) = i;
            return &gDeviceSlots[i].usbConn;
        }
    }

    return nullptr;
}

INTERNAL
void SetDeviceConn(u32 slot, Ds4Conn* conn)
{
    bool newConnection = true;
    Ds4Device* ds4 = &gDeviceSlots[slot];
    if (conn->type == Ds4Conn::TYPE_BT)
    {
        if (ds4->btConn.type != Ds4Conn::TYPE_NONE)
        {
            FinishDs4Conn(&ds4->btConn);
            newConnection = false;
        }
        memcpy(&ds4->btConn, conn, sizeof(Ds4Conn));
        //ds4->calibData = GetCalibData(conn);
    }
    else if (conn->type == Ds4Conn::TYPE_USB)
    {
        if (ds4->usbConn.type != Ds4Conn::TYPE_NONE)
        {
            FinishDs4Conn(&ds4->usbConn);
            newConnection = false;
        }
        memcpy(&ds4->usbConn, conn, sizeof(Ds4Conn));
        //ds4->calibData = GetCalibData(conn);
    }

    if (newConnection && gOnConnected)
    {
        gOnConnected(slot, ds4->mac, gExtra);
    }
}

void Ds4OnHidArrival(char* devPath)
{
    Ds4Conn conn = CreateDs4Conn(devPath);
    if (conn.type != Ds4Conn::TYPE_NONE)
    {
        u64 mac = GetMac(&conn);
        i32 slot = GetDeviceSlot(mac);
        if (slot == NO_DEVICE_SLOT)
        {
            slot = GetDeviceSlot(NO_DEVICE_MAC);
        }

        if (slot == NO_DEVICE_SLOT)
        {
            FinishDs4Conn(&conn);
            return;
        }

        gDeviceSlots[slot].mac = mac;
        SetDeviceConn(slot, &conn);
    }
}

void Ds4OnHidRemoval(char* devPath)
{
    u32 slot;
    Ds4Conn* conn = GetDs4Conn(devPath, &slot);
    if (conn)
    {
        FinishDs4Conn(conn);
        if (gOnDisconnected) gOnDisconnected(slot, gDeviceSlots[slot].mac, gExtra);
        //Release slot if there is no more connection
        if (gDeviceSlots[slot].btConn.type == Ds4Conn::TYPE_NONE && 
            gDeviceSlots[slot].usbConn.type == Ds4Conn::TYPE_NONE)
        {
            gDeviceSlots[slot].mac = 0;
        }
    }
}

void Ds4Init(Ds4OnConnected onConnected, Ds4OnDisconnected onDisconnected, void* extra)
{
    memset(gDeviceSlots, 0x00, sizeof(gDeviceSlots));
    gOnConnected = onConnected;
    gOnDisconnected = onDisconnected;
    gExtra = extra;
    Ds4Enumerate();
}

void Ds4Finish()
{
    gOnConnected = NULL;
    gOnDisconnected = NULL;
    gExtra = NULL;
}

#pragma pack(push, 1)
struct Ds4InputReport0x01
{
    u8 lStickX;
    u8 lStickY;
    u8 rStickX;
    u8 rStickY;
    u8 buttons0; 
    u8 buttons1;
    u8 buttons2AndCounter;
    u8 lTrigger;
    u8 rTrigger;
};
#pragma pack(pop)

INTERNAL
Ds4InputReport0x01* Ds4ReadReport0x01(Ds4Device* ds4, u8 buffer[DS4_INPUT_REPORT_0x01_SIZE])
{
    if (ds4->btConn.type == Ds4Conn::TYPE_BT)
    {
        buffer[0] = DS4_INPUT_REPORT_0x01;
        BOOL success = HidD_GetInputReport(ds4->btConn.device, buffer, DS4_INPUT_REPORT_0x01_SIZE);
        return (success && buffer[0] == DS4_INPUT_REPORT_0x01) ? ((Ds4InputReport0x01*) &buffer[1]) : (NULL);
    }
    else if (ds4->usbConn.type == Ds4Conn::TYPE_USB)
    {
        DWORD bytesRead = 0;
        BOOL success = ReadFile(ds4->usbConn.device, buffer, DS4_INPUT_REPORT_0x01_SIZE, &bytesRead, NULL);
        return (success && buffer[0] == DS4_INPUT_REPORT_0x01 && bytesRead == DS4_INPUT_REPORT_0x01_SIZE) ? ((Ds4InputReport0x01*)&buffer[1]) : (NULL);
    }
    
    return NULL;
}

INTERNAL
u8 Ds4PowhatToDpad(u8 powhat)
{
    u8 dpad = (0x01 << ((powhat & 0x0E) >> 1));
    dpad |= (dpad << (powhat & 0x01));
    dpad |= (dpad > 0x0F);
    dpad *= (powhat != 0x08);
    dpad &= 0x0F;
    return dpad;
}

void Ds4ReadState(uint32_t slot, Ds4InState* state)
{
    (*state) = {};
    if (slot >= DS4_MAX_DEVICES)
    {
        return;
    }

    Ds4Device* ds4 = &gDeviceSlots[slot];
    if (ds4->mac)
    {
        u8 buffer[DS4_INPUT_REPORT_0x01_SIZE];
        Ds4InputReport0x01* report = Ds4ReadReport0x01(ds4, buffer);
        if (report)
        {
            state->buttons = 0;

            state->buttons = Ds4PowhatToDpad(report->buttons0 & 0x0F);
            state->buttons <<= 2;
            state->buttons |= (report->buttons2AndCounter & 0x03);
            state->buttons <<= 8;
            state->buttons |= report->buttons1;
            state->buttons <<= 8;
            state->buttons |= report->buttons0;

            state->lStickX = report->lStickX;
            state->lStickY = report->lStickY;
            state->rStickX = report->rStickX;
            state->rStickY = report->rStickY;
            state->lTrigger = report->lTrigger;
            state->rTrigger = report->rTrigger;
        }
    }
}

void Ds4WriteState(uint32_t slot, Ds4OutState* state)
{
    if (slot >= DS4_MAX_DEVICES)
    {
        return;
    }

    Ds4Device* ds4 = &gDeviceSlots[slot];
    if (ds4->mac)
    {
        if (ds4->btConn.type == Ds4Conn::TYPE_BT)
        {
            u8 report[DS4_OUTPUT_REPORT_0x11_SIZE + 1];
            memset(report, 0x00, sizeof(report));
            //upper nibble (A - DATA, 4 get report, 5 set report), lower nibble (1 input 2 output 3 feature)
            report[0] = 0xA2;   //HID header - calculate crc for this too, but dont send it
            report[1] = DS4_OUTPUT_REPORT_0x11;
            report[2] = POLL_INTERVAL_2MS;
            report[4] = state->flags;

            report[7] = state->motorRight;
            report[8] = state->motorLeft;
            report[9] = state->ledColorRed;
            report[10] = state->ledColorGreen;
            report[11] = state->ledColorBlue;
            report[12] = state->ledTimeOnIn10MsTicks;
            report[13] = state->ledTimeOffIn10MsTicks;

            Ds4BtFillCrc32(report);
            HidD_SetOutputReport(ds4->btConn.device, &report[1], sizeof(report) - 1);
        }
        else if (ds4->usbConn.type == Ds4Conn::TYPE_USB)
        {
            u8 report[DS4_OUTPUT_REPORT_0x05_SIZE];
            memset(report, 0x00, sizeof(report));
            
            report[0] = DS4_OUTPUT_REPORT_0x05;
            report[1] = state->flags;
            report[4] = state->motorRight;
            report[5] = state->motorLeft;
            report[6] = state->ledColorRed;
            report[7] = state->ledColorGreen;
            report[8] = state->ledColorBlue;
            report[9] = state->ledTimeOnIn10MsTicks;
            report[10] = state->ledTimeOffIn10MsTicks;

            DWORD bytesWritten;
            WriteFile(ds4->usbConn.device, report, sizeof(report), &bytesWritten, NULL);
        }
    }
}

#ifdef DS4_INPUT_XINPUT_FORMAT
void Ds4ReadState(uint32_t slot, XINPUT_STATE* state)
{
    (*state) = {};
    if (slot >= DS4_MAX_DEVICES)
    {
        return;
    }

    Ds4Device* ds4 = &gDeviceSlots[slot];
    if (ds4->mac)
    {
        u8 buffer[DS4_INPUT_REPORT_0x01_SIZE];
        Ds4InputReport0x01* report = Ds4ReadReport0x01(ds4, buffer);
        if (report)
        {
            //Invert Y axis
            report->lStickY = (255 - report->lStickY);
            report->rStickY = (255 - report->rStickY);

            state->dwPacketNumber = (report->buttons2AndCounter & 0xFC);
            state->Gamepad.wButtons = 0;

            state->Gamepad.bLeftTrigger = report->lTrigger;
            state->Gamepad.bRightTrigger = report->rTrigger;

            //NOTE : add 8 bit unsigned to 16 bit signed to get the full range[32767, -32768]
            state->Gamepad.sThumbLX = (((report->lStickX) << 8) - 32768) + report->lStickX;
            state->Gamepad.sThumbLY = (((report->lStickY) << 8) - 32768) + report->lStickY;
            state->Gamepad.sThumbRX = (((report->rStickX) << 8) - 32768) + report->rStickX;
            state->Gamepad.sThumbRY = (((report->rStickY) << 8) - 32768) + report->rStickY;

            u8 powhat = report->buttons0 & 0x0F;
            state->Gamepad.wButtons |= (powhat == 0 || powhat == 1 || powhat == 7) << 0;    //Up
            state->Gamepad.wButtons |= (powhat == 3 || powhat == 4 || powhat == 5) << 1;    //Down
            state->Gamepad.wButtons |= (powhat == 5 || powhat == 6 || powhat == 7) << 2;    //Left
            state->Gamepad.wButtons |= (powhat == 1 || powhat == 2 || powhat == 3) << 3;    //Right
            state->Gamepad.wButtons |= ((report->buttons1 & 0x20) != 0) << 4;               //Start
            state->Gamepad.wButtons |= ((report->buttons1 & 0x10) != 0) << 5;               //Back
            state->Gamepad.wButtons |= (report->buttons1 & 0xC0);                           //LeftThumb, RightThumb
            state->Gamepad.wButtons |= (report->buttons1 & 0x03) << 8;                      //LeftShoulder, RightShoulder
            state->Gamepad.wButtons |= ((report->buttons0 & 0x20) != 0) << 12;              //A
            state->Gamepad.wButtons |= ((report->buttons0 & 0x40) != 0) << 13;              //B
            state->Gamepad.wButtons |= ((report->buttons0 & 0x10) != 0) << 14;              //X
            state->Gamepad.wButtons |= ((report->buttons0 & 0x80) != 0) << 15;              //Y
        }
    }
}

void Ds4WriteState(uint32_t slot, XINPUT_VIBRATION* state)
{
    Ds4OutState oState = {};
    oState.flags = DS4_FLAG_SET_MOTOR;
    oState.motorLeft = (state->wLeftMotorSpeed >> 8);
    oState.motorRight = (state->wRightMotorSpeed >> 8);

    Ds4WriteState(slot, &oState);
}
#endif
