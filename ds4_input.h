#pragma once

#include <stdint.h>
#ifdef DS4_INPUT_XINPUT_FORMAT 
#include <xinput.h>
#endif 

typedef void (*Ds4OnConnected) (uint32_t slot, uint64_t mac, void* extra);
typedef void (*Ds4OnDisconnected) (uint32_t slot, uint64_t mac, void* extra);

#define DS4_BUTTON_DPAD_STATE       uint32_t(0x0000000F << 0)

#define DS4_BUTTON_SQUARE           uint32_t(0x00000001 << 4)
#define DS4_BUTTON_X                uint32_t(0x00000001 << 5)
#define DS4_BUTTON_CIRCLE           uint32_t(0x00000001 << 6)
#define DS4_BUTTON_TRIANGLE         uint32_t(0x00000001 << 7)

#define DS4_BUTTON_L1               uint32_t(0x00000001 << 8)
#define DS4_BUTTON_R1               uint32_t(0x00000001 << 9)
#define DS4_BUTTON_L2               uint32_t(0x00000001 << 10)
#define DS4_BUTTON_R2               uint32_t(0x00000001 << 11)

#define DS4_BUTTON_SHARE            uint32_t(0x00000001 << 12)
#define DS4_BUTTON_OPTIONS          uint32_t(0x00000001 << 13)
#define DS4_BUTTON_L3               uint32_t(0x00000001 << 14)
#define DS4_BUTTON_R3               uint32_t(0x00000001 << 15)

#define DS4_BUTTON_PS               uint32_t(0x00000001 << 16)
#define DS4_BUTTON_TOUCHPAD         uint32_t(0x00000001 << 17)

#define DS4_BUTTON_UP               uint32_t(0x00000001 << 18)
#define DS4_BUTTON_RIGHT            uint32_t(0x00000001 << 19)
#define DS4_BUTTON_DOWN             uint32_t(0x00000001 << 20)
#define DS4_BUTTON_LEFT             uint32_t(0x00000001 << 21)

#define DS4_FLAG_SET_MOTOR          uint8_t(0x01)
#define DS4_FLAG_SET_LED_COLOR      uint8_t(0x02)
#define DS4_FLAG_SET_LED_TIME       uint8_t(0x04)

struct Ds4OutState
{
    uint8_t flags;
    uint8_t motorLeft;
    uint8_t motorRight;
    uint8_t ledColorRed;
    uint8_t ledColorGreen;
    uint8_t ledColorBlue;
    uint8_t ledTimeOnIn10MsTicks;
    uint8_t ledTimeOffIn10MsTicks;
};

struct Ds4InState
{
    uint32_t buttons;
    uint8_t lStickX;
    uint8_t lStickY;
    uint8_t rStickX;
    uint8_t rStickY;
    uint8_t lTrigger;
    uint8_t rTrigger;
};

void Ds4Init(Ds4OnConnected onConnected, Ds4OnDisconnected onDisconnected, void* extra);
void Ds4Finish();
void Ds4OnHidArrival(char* path);
void Ds4OnHidRemoval(char* path);
void Ds4ReadState(uint32_t slot, Ds4InState* state);
void Ds4WriteState(uint32_t slot, Ds4OutState* state);

#ifdef DS4_INPUT_XINPUT_FORMAT
void Ds4ReadState(uint32_t slot, XINPUT_STATE* state);
void Ds4WriteState(uint32_t slot, XINPUT_VIBRATION* state);
#endif
